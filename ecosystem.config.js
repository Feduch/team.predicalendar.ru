module.exports = {
    apps : [{
        name: 'team',
        script: "serve dist -p 8003",
        args: '',
        instances: 1,
        autorestart: true,
        watch: false,
        max_memory_restart: '1G'
    },
    {
        name: 'team-db',
        script: "json-server --watch db.json",
        args: '',
        instances: 1,
        autorestart: true,
        watch: false,
        max_memory_restart: '128M'
    }]
};
